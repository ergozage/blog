# [ErgoZage Blog](https://ergozage.gitlab.io/blog/)

I was looking for a simples way to start a blog, <b>my requirements are</b>:

0. I believe that text-based command/config is always superior to nice UI. 
I don't want to spend a bunch of time configuring and constantly look at some website/IU, clicking buttons, etc. 
Just want to git push my changes and the webpage is live with zero maintenance!
1. I want to have my own webpage where I host a blog with total control now and in the future with zero compromises and very little cost to set up!
2. I don't want to use any kind of 'simple' WordPress or anything that has some 'blog management' feature inside or just generates plain static html pages from UI.
3. I don't want to be a part of some blogging platform like Medium that pushes content upfront but suppresses its author. I want to be really on my own with total control and visibility.
4. I don't want any dynamic feature on my blog, like any backing database and/or the commenting system. Simple static pages are going to do just fine! I plan to link each blog with a post on some discussion network no need to reinvent the wheel.
5. I am not against paying for hosting/domains etc but would like to <b>NOT</b> deal with that as there is a strain from managing it, paying each month, renewing the domain, etc. Just don't want to deal with that, thanks!

## <i>So here is what I come up with after some research: </i>

[Svelte](https://svelte.dev/) based static blog hosted by [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

<b>npm run build</b> - compiles everything in a public folder that is later checked into master and that's your static blog webpage. Started by cloning [sveltejs/template](https://github.com/sveltejs/template). 

<b>npm i</b> - installs all dependencies (use after fresh git clone).
<b>npm run dev</b> - for local development.

<b>Why Svelte?</b> I have chosen Svelte as I needed some very simple framework with minimum complexity and very close to HTML/JS/CSS but still offers components that can reduce duplication and complexity. You can understand Svelte and start being productive in just one day! Could you do this in plain html/css/javascript? SURE!

<b>How to handle routes?</b> You can do routing on the 'client' side, meaning in the browser. For that, I have opted to use [svelte-spa-router](https://github.com/ItalyPaleAle/svelte-spa-router). I am not an expert so maybe there is a better way to do this, simpler. It was very easy to use this library just added [routes.js](routes.js) and used that in [App.Svelte](App.svelte). The only little thing is that it gives hash-based routes (#/goal).

<b>How to make Svelte complied static webpage work with GitLab pages</b>, after you get template change [index.html](https://github.com/sveltejs/template/blob/master/public/index.html) make sure you have relative paths (./) and not root ones (/) for href=* and src=*. 
The issue manifests itself as <b>X-Content-Type-Options=nosniff mismatch</b> but in reality, you will have wrong paths from index.html to other files (.css, .js, etc).

<b>RSS feed</b> is just a simple XML file that contains webpage content and is changed when a new post is added. For this, I have changed [rollup.config.js](rollup.config.js) by adding logic that will on prod build generate the file based on [BlogPostLookups.js](BlogPostLookups.js).

<b>Publishing of the blog is done via</b> [.gitlab-ci.yml](https://gitlab.com/ergozage/blog/-/blob/public_pages/.gitlab-ci.yml) and config is set up to listen for change on [publish_pages](https://gitlab.com/ergozage/blog/-/tree/public_pages) branch. I can perform code changes & build new blog iteratively by pushing small chunks to the Master branch. 
Once I am ready to publish something to the blog's webpage I run [deploy_pages.sh](deploy_pages.sh) and everything is live in around 20sec, it's that simple!
The job of compiling Svelte app to static html/js/css is done by my computer so GitLab just publishes whatever is already in the [public](public/) folder! This could be setup differently so .gitlab.ci.yml does the compilation but I wanted to remove strain from GitLab servers as this is a free service that works nice!

>Just push everything to Git repo and that's all - you get all the bells and whistles without the additional strain of paying money and managing hosting/security/domains(you can bring your own domain instead of using GitLab's ones)

## Copyright

Code and configuration around the blog content you can use however you see fit, copy it, modify, etc.

All of the blog content is copyright to ErgoZage and shouldn't be copied/changed/used in any shape or form. 
The blog is hosted via GitLab pages: https://ergozage.gitlab.io/blog/ and shouldn't be hosted by anyone anywhere else. Thanks!