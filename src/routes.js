import Home from './blog_posts/_Home.svelte'
import Goal from './blog_posts/03_21_21_Goal.svelte'
import HowToBlogEasy from './blog_posts/03_22_21_HowToBlogEasy.svelte'
import EngineeringIdeas from './blog_posts/03_27_21_EngineeringIdeas.svelte'
import Ergonomics from './blog_posts/04_11_21_Ergonomics.svelte'
import DeclutterAroundYou from './blog_posts/07_02_21_DeclutterAroundYou.svelte'

import NotFound from './notFound.svelte'

// Export the route definition object
export default {
    // Exact path
    '/': Home,
    '/Goal': Goal,
    '/EasyBlogging' : HowToBlogEasy,
    '/EngineeringIdeas' : EngineeringIdeas,
    '/Ergonomics' : Ergonomics,
    '/DeclutterAroundYou' : DeclutterAroundYou,

    // Catch-all, must be last
    '*': NotFound,
}