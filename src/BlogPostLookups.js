'use strict';

let blogPosts = [] 

blogPosts[blogPosts.length] = {
    postDate: new Date(2021, 7, 2),
    postDateStr: '07/july/2021',
    postTitle: 'Remove obstacles',
    postLocation: 'DeclutterAroundYou',
    shortDesc: 'Around you there are a lot of "obstacles" - items that stay on the forefront of your mind. You have to maintain and deal with them.'
}

blogPosts[blogPosts.length] = {
    postDate: new Date(2021, 3, 11),
    postDateStr: '11/apr/2021',
    postTitle: 'Ergonomics',
    postLocation: 'Ergonomics',
    shortDesc: 'My experience so far with ergonomics in life. Good tips on how to setup your workspace!'
}

blogPosts[blogPosts.length] = {
    postDate: new Date(2021, 2, 27),
    postDateStr: '27/mar/2021',
    postTitle: 'Ideas in Engineering',
    postLocation: 'EngineeringIdeas',
    shortDesc: 'How to properly nurture Ideas!'
}

blogPosts[blogPosts.length] = {
    postDate: new Date(2021, 2, 22),
    postDateStr: '22/mar/2021',
    postTitle: '"Really smart" blogging',
    postLocation: 'EasyBlogging',
    shortDesc: 'Blogging with GIT, how I do it as a Software Engineer.'
}

blogPosts[blogPosts.length] = {
    //Date.UTC(year, month, day) - month start from 0
    postDate: new Date(2021, 2, 21),
    postDateStr: '21/mar/2021',
    postTitle: 'My Goal - First blog post!',
    postLocation: 'Goal',
    shortDesc: 'What is my goal for starting a blog?'
}

module.exports = blogPosts 
