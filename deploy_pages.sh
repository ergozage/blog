#Make sure you do chmod +x deploy_pages.sh so you can run this script!

#make a copy of master build folder & switch to publish_pages branch
cp -r public/ public_temp/
git checkout public_pages
#now replace build folder with build_temp & clear
rm -r public/
cp -r public_temp/ public/
rm -r public_temp/
#now stage, commit & push everything and get back to master 
git add .
git commit -m 'Publishing new gitlab pages!'
git push
git checkout master