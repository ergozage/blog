import svelte from 'rollup-plugin-svelte';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import css from 'rollup-plugin-css-only';

import fs from 'fs';
import { Feed } from 'feed';

const production = !process.env.ROLLUP_WATCH;
const rootOutput = 'public/';

function serve() {
	let server;

	function toExit() {
		if (server) server.kill(0);
	}

	return {
		writeBundle() {
			if (server) return;
			server = require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
				stdio: ['ignore', 'inherit', 'inherit'],
				shell: true
			});

			process.on('SIGTERM', toExit);
			process.on('exit', toExit);
		}
	};
}

function genRss() {
	const blogPosts = require('./src/BlogPostLookups.js')
	const blogUrl = 'https://ergozage.gitlab.io/blog/';

	const feed = new Feed({
		title: 'ErgoZage blog',
		description: 'My safe place to help me with learning new things, track progress, rant, be smart and help others!',
		id: 'https://ergozage.gitlab.io/blog',
		link: 'https://ergozage.gitlab.io/blog',
		language: 'en',
		copyright: 'All rights reserved 2021, ErgoZage',
		favicon: 'https://ergozage.gitlab.io/blog/favicon.ico',
		feedLinks: {
			rss: blogUrl + 'rss.xml'
		},
		updated: blogPosts[0].postDate
	})

	for (let bp of blogPosts) {
		feed.addItem({
			title: bp.postTitle,
			id: blogUrl + '#/' + bp.postLocation,
			link: blogUrl + '#/' + bp.postLocation,
			description: bp.shortDesc
		})
	}

	fs.writeFile(rootOutput + 'rss.xml', feed.rss2({indent: true}), function (err) {
	  if (err) return console.log('Generate Rss:' + err);
	})
}

export default {
	input: 'src/main.js',
	output: {
		sourcemap: true,
		format: 'iife',
		name: 'app',
		file: rootOutput + 'build/bundle.js'
	},
	plugins: [
		svelte({
			compilerOptions: {
				// enable run-time checks when not in production
				dev: !production
			}
		}),
		// we'll extract any component CSS out into
		// a separate file - better for performance
		css({ output: 'bundle.css' }),

		// If you have external dependencies installed from
		// npm, you'll most likely need these plugins. In
		// some cases you'll need additional configuration -
		// consult the documentation for details:
		// https://github.com/rollup/plugins/tree/master/packages/commonjs
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		commonjs(),

		// In dev mode, call `npm run start` once
		// the bundle has been generated
		!production && serve(),

		// Watch the `public` directory and refresh the
		// browser on changes when not in production
		!production && livereload('public'),

		// If we're building for production (npm run build
		// instead of npm run dev), minify
		production && terser(),
		production && genRss()
	],
	watch: {
		clearScreen: false
	}
};
